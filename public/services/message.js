const MessageService = {
    PostMessage: async function(msg, senderID, chatID) {
        msgBody = {
            'msg': msg,
            'sender_id': senderID,
            'chat_id': chatID
        }
        const msgBlob = new Blob([JSON.stringify(msgBody, null, 2)], {type : 'application/json'});
        myInit = {
            method: 'POST',
            body: msgBlob
        }
        fetch(`${API_URL}messages`, myInit)
    }
}