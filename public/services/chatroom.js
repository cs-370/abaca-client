const ChatroomService = {
    GetMessagesForChatroom: async function(id){
        return await fetch(`${API_URL}chatrooms/${id}/messages`).then(
            async r => {
                if(r.status === 200){
                    return await r.json()
                }
            }
        );
    },

}