

const UserService = {

    GetCurrentUserInfo: async function(){
        return await fetch(`${API_URL}auth/verify`).then(
            async r => {
                if(r.status === 200){
                    return await r.json()
                }
            }
        );
    },

    GetCurrentUsername: async function(id) {
        return await fetch(`${API_URL}users/${id}`).then(
            async r => {
                if (r.status === 200) {
                    return await r.json()
                }
            }
        )
    },

    GetChatrooms: async function(id) {
        return await fetch(`${API_URL}users/${id}/chatrooms`).then(
            async r => {
                if (r.status === 200) {
                    return await r.json()
                }
            }
        )
    }
}